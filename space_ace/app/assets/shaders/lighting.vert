#version 330 core
layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inTexCoord;

out VS_OUT {
   out vec2 texCoord;
   out vec3 fragPos;
   out vec3 fragNormal;
} vs_out;

uniform mat4 model; //TODO: Combine these if all are not required
uniform mat4 view;
uniform mat4 proj;

void main()
{
   gl_Position = proj * view * model * vec4(inPos, 1.0);

   vs_out.fragPos = vec3(model * vec4(inPos, 1.0));
   vs_out.fragNormal = normalize(mat3(model) * inNormal);
   vs_out.texCoord = inTexCoord;
}