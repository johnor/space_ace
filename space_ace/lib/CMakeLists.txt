cmake_minimum_required (VERSION 3.14)

include(FetchContent)
FetchContent_Declare(
    SDL2
    GIT_REPOSITORY         https://github.com/SDL-mirror/SDL.git
    GIT_PROGRESS           TRUE
    GIT_TAG                release-2.0.12
    USES_TERMINAL_DOWNLOAD TRUE
)
FetchContent_Declare(
    ecs
    GIT_REPOSITORY         https://gitlab.com/Wahlstrand/ecs.git
    GIT_PROGRESS           TRUE
    GIT_TAG                V0.1.8
    USES_TERMINAL_DOWNLOAD TRUE
)
FetchContent_Declare(
    glm
    GIT_REPOSITORY https://github.com/g-truc/glm.git
    GIT_PROGRESS   TRUE
    GIT_TAG        0.9.9.8
    USES_TERMINAL_DOWNLOAD TRUE
    PATCH_COMMAND git apply ${CMAKE_CURRENT_SOURCE_DIR}/patch/glm.patch #Until my fixes are merged and released
)
FetchContent_Declare(
    stb
    GIT_REPOSITORY         https://gitlab.com/Wahlstrand/stb_cmake.git
    GIT_PROGRESS           TRUE
    GIT_TAG                master
    USES_TERMINAL_DOWNLOAD TRUE
)

set(ASSIMP_BUILD_ALL_IMPORTERS_BY_DEFAULT OFF CACHE BOOL "" FORCE)
set(ASSIMP_BUILD_ALL_EXPORTERS_BY_DEFAULT OFF CACHE BOOL "" FORCE)
set(ASSIMP_BUILD_OBJ_IMPORTER ON CACHE BOOL "" FORCE)
set(ASSIMP_BUILD_TESTS OFF CACHE BOOL "" FORCE)                    #BAD, fail to compile with clang/c++20

FetchContent_Declare(
    assimp
    GIT_REPOSITORY         https://github.com/assimp/assimp.git
    GIT_PROGRESS           TRUE
    GIT_TAG                v5.0.1
    USES_TERMINAL_DOWNLOAD TRUE
)
FetchContent_MakeAvailable(SDL2)
FetchContent_MakeAvailable(ecs)
FetchContent_MakeAvailable(glm)
FetchContent_MakeAvailable(stb)
FetchContent_MakeAvailable(assimp)

target_compile_definitions(glm
   INTERFACE GLM_FORCE_SILENT_WARNINGS
)

add_subdirectory(flags)
add_subdirectory(core)
add_subdirectory(gfx)
