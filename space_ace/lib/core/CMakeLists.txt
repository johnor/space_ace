cmake_minimum_required (VERSION 3.14)

add_library(space_ace_core_lib INTERFACE)
add_library(sa::core ALIAS space_ace_core_lib)

target_include_directories (space_ace_core_lib INTERFACE include shaders sa::flags)
target_link_libraries(space_ace_core_lib INTERFACE glm::glm ecs assimp::assimp)
