#pragma once

namespace sa::core::component
{
struct SpaceshipPhysics
{
   //State
   float m_speed = 1.5f;
   float m_roll = 0.0f; //These are a bit redundant with pos but
   float m_yaw = 0.0f;  //it simplifies math and makes it easier to prevent
                        //angular drift

   //Specs
   float m_maxTurnRate = 1.0f / 60.0f;                    //1 deg/tick
   float m_maxRoll = 1.5707963267948966192313216916398f; //90 deg
   float m_responsiveness = 0.04f;

   //Controls
   enum class TurnCommand
   {
      left,
      right,
      none,
   };
   TurnCommand m_cmd = TurnCommand::none;
};
} // namespace sa::core::component