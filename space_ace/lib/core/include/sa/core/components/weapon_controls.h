#pragma once

namespace sa::core::component
{
	enum class WeaponCommand
	{
		idle,
		fire
	};
	struct WeaponControls
	{
		WeaponCommand m_cmd = WeaponCommand::idle;	
	};
}