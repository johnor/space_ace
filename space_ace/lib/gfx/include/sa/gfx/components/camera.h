#pragma once

#include <glad/glad.h>

#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>

namespace sa::gfx::component
{
   struct Camera
   {
      //Lens config
      float m_zNear;
      float m_zFar;
      float m_fov; //rad;

      //Positioning
      float m_distance = 200.0f;
      glm::quat m_defaultRot;
      glm::quat m_trackball = glm::quat(glm::vec3(0.0, 0.0, 0.0));

      //GUI interaction. Probably move this elsewhere?
      glm::ivec2 m_dragBegin = glm::ivec2();
      bool m_dragged = false;
   };
}