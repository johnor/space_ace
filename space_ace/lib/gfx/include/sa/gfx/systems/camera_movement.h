#pragma once

#include <glm/gtc/quaternion.hpp>
#include "ecs/isystem.h"
#include "ecs/single_entity_tracker.h"
#include "sa/core/components/pos.h"
#include "sa/gfx/components/camera.h"
#include "sa/gfx/components/camera_focus.h"

namespace sa::gfx::systems
{
template <typename ECST>
class CameraMovement : public ecs::ISystem
{
public:
   CameraMovement(ECST &ecs)
       : m_ecs(ecs), m_focusTracker(ecs), m_cameraTracker(ecs)
   {
   }
   void update() final
   {
      const auto cameraId = m_cameraTracker.track();
      auto& camera =  m_ecs.template getComponent<gfx::component::Camera>(cameraId);

      if(!camera.m_dragged)
      {
         const static glm::quat identity = glm::quat(glm::vec3());
         camera.m_trackball = glm::mix(camera.m_trackball, identity, 0.1f);
      }

      const auto camRot = camera.m_trackball * camera.m_defaultRot;
      const auto camForward = camRot * glm::vec3(1.0f, 0.0f, 0.0f);

      const auto& focusPos = m_ecs.template getComponent<core::component::Pos>(m_focusTracker.track());
      auto &cameraPos = m_ecs.template getComponent<core::component::Pos>(cameraId);
      cameraPos.m_quat = camRot;
      cameraPos.m_r = focusPos.m_r - (camForward * camera.m_distance);
   }

private:
   ECST &m_ecs;
   ecs::SingleEntityTracker<ECST, core::component::Pos, component::CameraFocus> m_focusTracker;
   ecs::SingleEntityTracker<ECST, core::component::Pos, component::Camera> m_cameraTracker;
};
} // namespace sa::gfx::systems