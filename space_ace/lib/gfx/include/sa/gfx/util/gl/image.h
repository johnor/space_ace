#pragma once

#include <memory>
#include <string>
#include <glad/glad.h>

namespace sa::gfx::gl
{
class Image final
{
public:
   using VoidDeleterT = void (*)(void *);
   Image(GLint width,
         GLint height,
         GLenum format,
         std::unique_ptr<void, VoidDeleterT>&& upData);
   Image(Image &&other);

   GLint width() const;
   GLint height() const;
   GLenum format() const;
   const void* data() const;

   static Image load(const std::string& path);

private:
   GLint m_width;
   GLint m_height;
   GLenum m_format;
   std::unique_ptr<void, VoidDeleterT> m_upData;
};
} // namespace sa::gl